const gauss_random = () => {
  const avg = 0.0;
  const sigma = 5.0;

  let haveNextGaussian = false;
  let nextGaussian = 0.0;

  if (haveNextGaussian) {
    haveNextGaussian = false;
    return nextGaussian * sigma + avg;
  } else {
    let v1, v2, s;
    do {
      v1 = (Math.random() - 0.5) * 2;
      v2 = (Math.random() - 0.5) * 2;
      s = v1 * v1 + v2 * v2;
    } while (s >= 1.0 || Math.abs(s) < Number.EPSILON);
    const multiplier = Math.sqrt((-2 * Math.log(s)) / s);
    nextGaussian = v2 * multiplier;
    haveNextGaussian = true;
    return v1 * multiplier * sigma + avg;
  }
};

const calculateAvgDmgs = () => {
  let schopka = gauss_random();
  schopka = Math.max(-30, schopka + 0.5);
  schopka = Math.min(30, schopka);

  let prumka = 0;
  if (Math.abs(schopka) <= 20) {
    prumka =
      -2 * schopka +
      Math.abs((Math.random() - 0.5) * 16 + (Math.random() - 0.5) * 16) +
      Math.random() * 3 +
      1;
  } else {
    prumka = -2 * schopka + Math.random() * 4 + 1;
  }

  return { prumka, schopka };
};

const PRUMKA_SCHOPKA = 0; // 0 = počítá průmku, 1 = počítá schopku

// Najde všechny průmky nad danou hodnotu s určitým počtem DM

// Hodnoty k nastavení
const dm = 0;
const reqprumka = 60;
const reqschopka = -3;
// Konec hodnot k nastavení

const hodnotyNadX = [];
let nejPrumka = 0;
let nejSchopka = 0;
for (let index = 0; index < Math.round(dm / 149) * 4; index++) {
  const { prumka, schopka } = calculateAvgDmgs();
  nejPrumka = Math.max(nejPrumka, prumka);
  nejSchopka = Math.max(nejSchopka, schopka);
  if (PRUMKA_SCHOPKA === 0) {
    if (prumka > reqprumka) {
      hodnotyNadX.push(prumka);
    }
  } else {
    if (schopka > reqschopka) {
      hodnotyNadX.push(schopka);
    }
  }
  // console.log('Průmka: ' +Math.round(prumka), 'Schopka: ' +Math.round(schopka),'Začarek: '+ index);
}

if (dm >= 149) {
  if (PRUMKA_SCHOPKA === 0) {
    console.log(`Průmky nad ${reqprumka}: ${hodnotyNadX.length}`);
  } else {
    console.log(`Schopky nad ${reqschopka}: ${hodnotyNadX.length}`);
  }
  console.log(`Zadaných DM: ${dm}`);
  console.log(`Použitých začarek: ${Math.round(dm / 149) * 4}`);
  console.log("--------------------------");
  console.log(`Největší průmka: ${parseInt(nejPrumka)}`);
  console.log(`Největší schopka: ${parseInt(nejSchopka)}`);
}

// Spočítá průměr natočení dané průmky
// pocetNatoceni - pocet testů - čím větší, tím přesnější

// Hodnoty k nastavení
const pocetNatoceni = 100000;
const pozadovanaPrumka = 40;
const pozadovanaSchopka = 15;
const ocekavanyVkladZačarek = 5;
const expect100Percantage = true;
// Konec hodnot k nastavení

let totalZacarka = 0;
let nejvetsiPrumka = 0;
let nejvetsiSchopka = 0;

let tmpChance = 0;
let amountOfSwapTo100 = 0;

let nejvetsiPocetZacarek = 0;
let nejmensiPocetZacarek = 0;
for (let i = 0; i < pocetNatoceni; i++) {
  let pPrumka = 0;
  let pSchopka = 0;
  let pocetZacarek = 0;

  do {
    const { prumka, schopka } = calculateAvgDmgs();
    pPrumka = prumka;
    pSchopka = schopka;
    pocetZacarek++;

    nejvetsiPrumka = Math.max(nejvetsiPrumka, pPrumka);
    nejvetsiSchopka = Math.max(nejvetsiSchopka, pSchopka);
    totalZacarka++;
  } while (
    PRUMKA_SCHOPKA === 0
      ? pPrumka <= pozadovanaPrumka
      : pSchopka <= pozadovanaSchopka
  );

  if (expect100Percantage) {
    tmpChance = 1 - Math.pow(1 - 1 / (totalZacarka / (i + 1)), totalZacarka);
    // console.log(tmpChance, totalZacarka);
    if (tmpChance > 0.9999 && !amountOfSwapTo100 && i > 0) {
      amountOfSwapTo100 = i;
    }
  }

  nejvetsiPocetZacarek = Math.max(nejvetsiPocetZacarek, pocetZacarek);
  if (!i) {
    nejmensiPocetZacarek = pocetZacarek;
  }
  nejmensiPocetZacarek = Math.min(nejmensiPocetZacarek, pocetZacarek);
}
if (pocetNatoceni > 0) {
  if (PRUMKA_SCHOPKA === 0) {
    console.log(
      "Průměrný počet začarek na natočení prumky " +
        pozadovanaPrumka +
        ": " +
        parseInt(totalZacarka / pocetNatoceni)
    );
  } else {
    console.log(
      "Průměrný počet začarek na natočení schopky " +
        pozadovanaSchopka +
        ": " +
        parseInt(totalZacarka / pocetNatoceni)
    );
  }
  console.log(
    `Šance na natočení na ${ocekavanyVkladZačarek} začarek: ${(
      (1 -
        Math.pow(
          1 - 1 / (totalZacarka / pocetNatoceni),
          ocekavanyVkladZačarek
        )) *
      100
    ).toFixed(4)}%`
  );
  if (amountOfSwapTo100) {
    console.log(
      `Potřebný počet začarek na natočení dané ${
        !PRUMKA_SCHOPKA ? "průmky" : "schopky"
      } na 100%: ${Math.round(
        (totalZacarka / pocetNatoceni) * amountOfSwapTo100
      )}`
    );
  } else {
    console.log("Nebyl použit potřebný počet začarek k výpočtu 100% šance");
  }
  console.log(
    "Průměrný počet DM: " +
      Math.ceil((Math.ceil(totalZacarka / pocetNatoceni) / 4) * 149)
  );
  console.log("Při počtu testů: " + pocetNatoceni);
  console.log("-------------------------------");
  console.log(
    `Největší počet začarek na natočení dané ${
      !PRUMKA_SCHOPKA ? "průmky" : "schopky"
    }: ${nejvetsiPocetZacarek}`
  );
  console.log(
    `Nejmenší počet začarek na natočení dané ${
      !PRUMKA_SCHOPKA ? "průmky" : "schopky"
    }: ${nejmensiPocetZacarek}`
  );
  console.log("-------------------------------");
  console.log(
    "Celkový počet použitých začarek ve všech testech: " + totalZacarka
  );
  console.log(
    "Celkový počet použitých DM ve všech testech: " +
      parseInt((totalZacarka / 4) * 149)
  );
  console.log(`Největší natočená průmka: ${parseInt(nejvetsiPrumka)}`);
  console.log(`Největší natočená schopka: ${parseInt(nejvetsiSchopka)}`);
}
